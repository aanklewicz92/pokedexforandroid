package com.example.pokedexforandroid.database.task;

import android.os.AsyncTask;

import com.example.pokedexforandroid.database.DatabaseHelper;
import com.example.pokedexforandroid.database.model.Coach;
import com.example.pokedexforandroid.utils.Constants;

public class CoachTask extends AsyncTask<Coach, Void, Boolean> {

    private IDatabaseOperationListener mListener;
    private int mOperation;

    public CoachTask(IDatabaseOperationListener listener, int operation) {
        mListener = listener;
        mOperation = operation;
    }

    @Override
    protected Boolean doInBackground(Coach... coaches) {
        return executeQuery(coaches[0]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        postExecutionResult(result);
    }

    private boolean executeQuery(Coach coach) {
        switch (mOperation) {
            case Constants.SAVE_COACH_TASK:
                return DatabaseHelper.getInstance().save(coach);
            case Constants.DELETE_COACH_TASK:
                DatabaseHelper.getInstance().delete(coach);
                return true;
            default:
                return false;
        }
    }

    private void postExecutionResult(Boolean result) {
        switch (mOperation) {
            case Constants.SAVE_COACH_TASK:
                mListener.onCoachSaved(result);
            case Constants.DELETE_COACH_TASK:
                mListener.onCoachDeleted();
        }
    }
}
