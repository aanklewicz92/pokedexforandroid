package com.example.pokedexforandroid.database.task;

import android.os.AsyncTask;

import com.example.pokedexforandroid.database.DatabaseHelper;
import com.example.pokedexforandroid.database.model.Pokemon;
import com.example.pokedexforandroid.utils.Constants;

public class PokemonTask extends AsyncTask<Pokemon, Void, Boolean> {

    private IDatabaseOperationListener mListener;
    private int mOperation;

    public PokemonTask(IDatabaseOperationListener listener, int operation) {
        mListener = listener;
        mOperation = operation;
    }

    @Override
    protected Boolean doInBackground(Pokemon... pokemon) {
        return executeQuery(pokemon[0]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        postExecutionResult(result);
    }

    private boolean executeQuery(Pokemon pokemon) {
        switch (mOperation) {
            case Constants.SAVE_POKEMON_TASK:
                return DatabaseHelper.getInstance().save(pokemon);
            case Constants.DELETE_POKEMON_TASK:
                DatabaseHelper.getInstance().delete(pokemon);
                return true;
            default:
                return false;
        }
    }

    private void postExecutionResult(Boolean result) {
        switch (mOperation) {
            case Constants.SAVE_POKEMON_TASK:
                mListener.onPokemonSaved(result);
            case Constants.DELETE_POKEMON_TASK:
                mListener.onPokemonDeleted();
        }
    }
}
