package com.example.pokedexforandroid.database.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.util.ArrayList;
import java.util.List;

@Table(name = "Coach")
public class Coach extends Model {

    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private List<PokemonCoach> getPokemonCoach() {
        return getMany(PokemonCoach.class, "Coach");
    }

    public List<Pokemon> getPokemons() {
        List<Pokemon> pokemons = new ArrayList<>();
        List<PokemonCoach> pokemonCoaches = getPokemonCoach();
        for (PokemonCoach pokemonCoach : pokemonCoaches)
            pokemons.add(pokemonCoach.getPokemon());
        return pokemons;
    }
}
