package com.example.pokedexforandroid.database.task;

import android.os.AsyncTask;

import com.example.pokedexforandroid.database.DatabaseHelper;
import com.example.pokedexforandroid.database.model.Coach;

import java.util.List;

public class CoachesTask extends AsyncTask<Void, Void, List<Coach>> {

    private IDatabaseOperationListener mListener;

    public CoachesTask(IDatabaseOperationListener listener) {
        mListener = listener;
    }

    @Override
    protected List<Coach> doInBackground(Void... params) {
        return DatabaseHelper.getInstance().getAllCoaches();
    }

    @Override
    protected void onPostExecute(List<Coach> coaches) {
        super.onPostExecute(coaches);
        mListener.onCoachesSelected(coaches);
    }
}