package com.example.pokedexforandroid.database.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "Pokemon")
public class Pokemon extends Model {

    @Column(name = "name")
    private String name;
    @Column(name = "level")
    private int level;
    @Column(name = "type")
    private Type type;
    @Column(name = "image_src")
    private String imageSrc;

    public int getLevel() {
        return level;
    }

    public Type getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public void setImageSrc(String imageSrc) {
        this.imageSrc = imageSrc;
    }

    public enum Type {
        GRASS, FIRE, WATER, ELECTRIC
    }
}
