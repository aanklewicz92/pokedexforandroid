package com.example.pokedexforandroid.database.task;

import com.example.pokedexforandroid.database.model.Coach;
import com.example.pokedexforandroid.database.model.Pokemon;

import java.util.List;

public interface IDatabaseOperationListener {
    void onPokemonSaved(Boolean result);
    void onCoachSaved(Boolean result);
    void onPokemonCoachSaved(Boolean result);

    void onPokemonDeleted();
    void onCoachDeleted();
    void onPokemonCoachDeleted();

    void onPokemonsSelected(List<Pokemon> pokemons);
    void onCoachesSelected(List<Coach> coaches);
}
