package com.example.pokedexforandroid.database;

import com.activeandroid.query.Select;
import com.example.pokedexforandroid.database.model.Coach;
import com.example.pokedexforandroid.database.model.Pokemon;
import com.example.pokedexforandroid.database.model.PokemonCoach;

import java.util.List;

public class DatabaseHelper {

    private static DatabaseHelper databaseHelper;

    private DatabaseHelper() {}

    public static DatabaseHelper getInstance() {
        if(databaseHelper == null) {
            databaseHelper = new DatabaseHelper();
        }

        return databaseHelper;
    }

    public boolean save(Pokemon pokemon) {
        return pokemon.save() > -1;
    }

    public boolean save(Coach coach) {
        return coach.save() > -1;
    }

    public boolean save(PokemonCoach pokemonCoach) {
        return pokemonCoach.save() > -1;
    }

    public List<Coach> getAllCoaches() {
        return new Select().from(Coach.class).execute();
    }

    public List<Pokemon> getAllPokemons() {
        return new Select().from(Pokemon.class).execute();
    }

    public void delete(Pokemon pokemon) {
        List<PokemonCoach> pokemonCoaches = new Select().from(PokemonCoach.class).where("Pokemon = ?", pokemon.getId()).execute();
        for(PokemonCoach pokemonCoach : pokemonCoaches)
            pokemonCoach.delete();
        pokemon.delete();
    }

    public void delete(Coach coach) {
        List<PokemonCoach> pokemonCoaches = new Select().from(PokemonCoach.class).where("Coach = ?", coach.getId()).execute();
        for(PokemonCoach pokemonCoach : pokemonCoaches)
            pokemonCoach.delete();
        coach.delete();
    }

    public void delete(PokemonCoach pokemonCoach) {
        pokemonCoach.delete();
    }
}
