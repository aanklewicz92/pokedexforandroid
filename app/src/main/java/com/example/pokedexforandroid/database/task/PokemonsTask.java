package com.example.pokedexforandroid.database.task;

import android.os.AsyncTask;

import com.example.pokedexforandroid.database.DatabaseHelper;
import com.example.pokedexforandroid.database.model.Pokemon;

import java.util.List;

public class PokemonsTask extends AsyncTask<Void, Void, List<Pokemon>> {

    private IDatabaseOperationListener mListener;

    public PokemonsTask(IDatabaseOperationListener listener) {
        mListener = listener;
    }

    @Override
    protected List<Pokemon> doInBackground(Void... params) {
        return DatabaseHelper.getInstance().getAllPokemons();
    }

    @Override
    protected void onPostExecute(List<Pokemon> pokemons) {
        super.onPostExecute(pokemons);
        mListener.onPokemonsSelected(pokemons);
    }
}