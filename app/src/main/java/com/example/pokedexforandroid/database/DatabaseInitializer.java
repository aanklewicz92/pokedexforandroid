package com.example.pokedexforandroid.database;

import com.example.pokedexforandroid.database.model.Coach;
import com.example.pokedexforandroid.database.model.Pokemon;
import com.example.pokedexforandroid.database.model.PokemonCoach;
import com.example.pokedexforandroid.database.task.CoachTask;
import com.example.pokedexforandroid.database.task.IDatabaseOperationListener;
import com.example.pokedexforandroid.database.task.PokemonCoachTask;
import com.example.pokedexforandroid.database.task.PokemonTask;
import com.example.pokedexforandroid.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class DatabaseInitializer implements IDatabaseOperationListener {
    private ArrayList<Pokemon> mPokemons = new ArrayList<>();
    private ArrayList<Coach> mCoaches = new ArrayList<>();
    private ArrayList<PokemonCoach> mPokemonCoaches = new ArrayList<>();

    private int mSavedPokemonsCount = 0;
    private int mSavedCoachesCount = 0;
    private int mSavedPokemonsCoachesCount = 0;

    private Boolean mResult = true;

    private IDatabaseInitializerListener mListener;

    public DatabaseInitializer(IDatabaseInitializerListener listener) {
        mListener = listener;
    }

    public void initializeDatabase() {
        initializePokemons();
        initializeCoaches();
        initializePokemonsCoaches();
        savePokemons();
    }

    private void initializePokemons() {
        Pokemon pokemon = new Pokemon();
        pokemon.setName("Pikachu");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328192412/pokemon/images/0/0d/025Pikachu.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Raichu");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img4.wikia.nocookie.net/__cb20140328192425/pokemon/images/8/88/026Raichu.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Magnemite");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328204314/pokemon/images/6/6c/081Magnemite.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Magneton");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img4.wikia.nocookie.net/__cb20140328204316/pokemon/images/7/72/082Magneton.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Voltorb");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img1.wikia.nocookie.net/__cb20140328204636/pokemon/images/d/da/100Voltorb.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Electrode");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328205301/pokemon/images/8/84/101Electrode.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Electabuzz");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328210351/pokemon/images/d/de/125Electabuzz.pngg");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Magmar");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328210351/pokemon/images/8/8c/126Magmar.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Jolteon");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328210733/pokemon/images/b/bb/135Jolteon.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Flareon");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328210733/pokemon/images/d/dd/136Flareon.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Zapdos");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setImageSrc("http://img4.wikia.nocookie.net/__cb20140328211202/pokemon/images/e/e3/145Zapdos.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Moltres");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328211203/pokemon/images/1/1b/146Moltres.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Charmander");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140724195345/pokemon/images/7/73/004Charmander.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Charmeleon");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img1.wikia.nocookie.net/__cb20140328191309/pokemon/images/4/4a/005Charmeleon.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Charizard");
        pokemon.setLevel(3);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img4.wikia.nocookie.net/__cb20140328191325/pokemon/images/7/7e/006Charizard.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Growlithe");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328195857/pokemon/images/3/3d/058Growlithe.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Arcanine");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.FIRE);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328195858/pokemon/images/b/b8/059Arcanine.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Bulbasaur");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328190757/pokemon/images/2/21/001Bulbasaur.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Ivvsaur");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img1.wikia.nocookie.net/__cb20140328190847/pokemon/images/7/73/002Ivysaur.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Venusaur");
        pokemon.setLevel(3);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328190902/pokemon/images/a/ae/003Venusaur.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Oddish");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328194044/pokemon/images/4/43/043Oddish.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Gloom");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img1.wikia.nocookie.net/__cb20140328194044/pokemon/images/2/2a/044Gloom.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Vileplume");
        pokemon.setLevel(3);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328194044/pokemon/images/6/6a/045Vileplume.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Bellsprout");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328203359/pokemon/images/a/a2/069Bellsprout.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Weepinbell");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img4.wikia.nocookie.net/__cb20140328203359/pokemon/images/9/9f/070Weepinbell.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Victreebel");
        pokemon.setLevel(3);
        pokemon.setType(Pokemon.Type.GRASS);
        pokemon.setImageSrc("http://img1.wikia.nocookie.net/__cb20140328203937/pokemon/images/b/be/071Victreebel.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Squirtle");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img4.wikia.nocookie.net/__cb20140328191525/pokemon/images/3/39/007Squirtle.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Wartortle");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328191553/pokemon/images/0/0c/008Wartortle.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Blastoise");
        pokemon.setLevel(3);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img1.wikia.nocookie.net/__cb20140328191642/pokemon/images/0/02/009Blastoise.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Psyduck");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img2.wikia.nocookie.net/__cb20140328195856/pokemon/images/5/53/054Psyduck.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Golduck");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328195856/pokemon/images/f/fe/055Golduck.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Horsea");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328205800/pokemon/images/5/5a/116Horsea.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Seadra");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328205801/pokemon/images/2/26/117Seadra.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Seel");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328204317/pokemon/images/1/1f/086Seel.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Dewgong");
        pokemon.setLevel(2);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img3.wikia.nocookie.net/__cb20140328204318/pokemon/images/c/c7/087Dewgong.png");
        mPokemons.add(pokemon);

        pokemon = new Pokemon();
        pokemon.setName("Lapras");
        pokemon.setLevel(1);
        pokemon.setType(Pokemon.Type.WATER);
        pokemon.setImageSrc("http://img1.wikia.nocookie.net/__cb20140328210730/pokemon/images/a/ab/131Lapras.png");
        mPokemons.add(pokemon);
    }

    private void initializeCoaches() {
        Coach coach = new Coach();
        coach.setName("Ash");
        mCoaches.add(coach);

        coach = new Coach();
        coach.setName("Brock");
        mCoaches.add(coach);
    }

    private void initializePokemonsCoaches() {
        PokemonCoach pokemonCoach = new PokemonCoach();
        pokemonCoach.setPokemon(mPokemons.get(0));
        pokemonCoach.setCoach(mCoaches.get(0));
        mPokemonCoaches.add(pokemonCoach);

        pokemonCoach = new PokemonCoach();
        pokemonCoach.setPokemon(mPokemons.get(1));
        pokemonCoach.setCoach(mCoaches.get(0));
        mPokemonCoaches.add(pokemonCoach);

        pokemonCoach = new PokemonCoach();
        pokemonCoach.setPokemon(mPokemons.get(2));
        pokemonCoach.setCoach(mCoaches.get(0));
        mPokemonCoaches.add(pokemonCoach);

        pokemonCoach = new PokemonCoach();
        pokemonCoach.setPokemon(mPokemons.get(1));
        pokemonCoach.setCoach(mCoaches.get(1));
        mPokemonCoaches.add(pokemonCoach);

        pokemonCoach = new PokemonCoach();
        pokemonCoach.setPokemon(mPokemons.get(2));
        pokemonCoach.setCoach(mCoaches.get(1));
        mPokemonCoaches.add(pokemonCoach);

        pokemonCoach = new PokemonCoach();
        pokemonCoach.setPokemon(mPokemons.get(3));
        pokemonCoach.setCoach(mCoaches.get(1));
        mPokemonCoaches.add(pokemonCoach);
    }

    private void savePokemons() {
        for (Pokemon pokemon : mPokemons)
            new PokemonTask(this, Constants.SAVE_POKEMON_TASK).execute(pokemon);
}

    private void saveCoaches() {
        for (Coach coach : mCoaches)
            new CoachTask(this, Constants.SAVE_COACH_TASK).execute(coach);
    }

    private void savePokemonsCoaches() {
        for (PokemonCoach pokemonCoach : mPokemonCoaches)
            new PokemonCoachTask(this, Constants.SAVE_POKEMON_COACH_TASK).execute(pokemonCoach);
    }

    @Override
    public void onPokemonSaved(Boolean result) {
        mSavedPokemonsCount++;
        mResult = result;
        if(mSavedPokemonsCount >= mPokemons.size() && mResult)
            saveCoaches();
    }

    @Override
    public void onCoachSaved(Boolean result) {
        mSavedCoachesCount++;
        mResult = result;
        if(mSavedCoachesCount >= mCoaches.size() && mResult)
            savePokemonsCoaches();
    }

    @Override
    public void onPokemonCoachSaved(Boolean result) {
        mSavedPokemonsCoachesCount++;
        mResult = result;
        if(mSavedPokemonsCoachesCount >= mPokemonCoaches.size())
            mListener.databaseInitialized(mResult);
    }

    @Override
    public void onPokemonDeleted() {}

    @Override
    public void onCoachDeleted() {}

    @Override
    public void onPokemonCoachDeleted() {}

    @Override
    public void onPokemonsSelected(List<Pokemon> pokemons) {}

    @Override
    public void onCoachesSelected(List<Coach> coaches) {}
}
