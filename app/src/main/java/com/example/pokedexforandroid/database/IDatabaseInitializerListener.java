package com.example.pokedexforandroid.database;

public interface IDatabaseInitializerListener {
    void databaseInitialized(Boolean result);
}
