package com.example.pokedexforandroid.database.task;

import android.os.AsyncTask;

import com.example.pokedexforandroid.database.DatabaseHelper;
import com.example.pokedexforandroid.database.model.PokemonCoach;
import com.example.pokedexforandroid.utils.Constants;

public class PokemonCoachTask extends AsyncTask<PokemonCoach, Void, Boolean> {

    private IDatabaseOperationListener mListener;
    private int mOperation;

    public PokemonCoachTask(IDatabaseOperationListener listener, int operation) {
        mListener = listener;
        mOperation = operation;
    }

    @Override
    protected Boolean doInBackground(PokemonCoach... pokemonCoaches) {
        return executeQuery(pokemonCoaches[0]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        postExecutionResult(result);
    }

    private boolean executeQuery(PokemonCoach pokemonCoach) {
        switch (mOperation) {
            case Constants.SAVE_POKEMON_COACH_TASK:
                return DatabaseHelper.getInstance().save(pokemonCoach);
            case Constants.DELETE_POKEMON_COACH_TASK:
                DatabaseHelper.getInstance().delete(pokemonCoach);
                return true;
            default:
                return false;
        }
    }

    private void postExecutionResult(Boolean result) {
        switch (mOperation) {
            case Constants.SAVE_POKEMON_COACH_TASK:
                mListener.onPokemonCoachSaved(result);
            case Constants.DELETE_POKEMON_COACH_TASK:
                mListener.onPokemonCoachDeleted();
        }
    }
}