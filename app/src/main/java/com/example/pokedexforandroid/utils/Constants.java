package com.example.pokedexforandroid.utils;

public class Constants {
    public static final String SHARED_PREFS_DATABASE_EXISTS = "shared_prefs_database_exists";

    public static final int SAVE_POKEMON_TASK = 1;
    public static final int SAVE_COACH_TASK = 2;
    public static final int SAVE_POKEMON_COACH_TASK = 3;

    public static final int DELETE_POKEMON_TASK = 4;
    public static final int DELETE_COACH_TASK = 5;
    public static final int DELETE_POKEMON_COACH_TASK = 6;
}
