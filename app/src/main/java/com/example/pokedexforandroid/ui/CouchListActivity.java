package com.example.pokedexforandroid.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pokedexforandroid.R;
import com.example.pokedexforandroid.database.DatabaseHelper;
import com.example.pokedexforandroid.database.DatabaseInitializer;
import com.example.pokedexforandroid.database.IDatabaseInitializerListener;
import com.example.pokedexforandroid.database.model.Coach;
import com.example.pokedexforandroid.database.model.Pokemon;
import com.example.pokedexforandroid.database.model.PokemonCoach;
import com.example.pokedexforandroid.database.task.CoachTask;
import com.example.pokedexforandroid.database.task.CoachesTask;
import com.example.pokedexforandroid.database.task.IDatabaseOperationListener;
import com.example.pokedexforandroid.database.task.PokemonCoachTask;
import com.example.pokedexforandroid.database.task.PokemonTask;
import com.example.pokedexforandroid.database.task.PokemonsTask;
import com.example.pokedexforandroid.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CouchListActivity extends AppCompatActivity implements IDatabaseInitializerListener, IDatabaseOperationListener {

    @Bind(R.id.text_view)
    TextView pokemonTextView;

    @Bind(R.id.image_view)
    ImageView imageView;

    private Pokemon mPokemon;
    private Coach mCoach;
    private PokemonCoach mPokemonCoach;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_couch_list);
        ButterKnife.bind(this);

        onClickGetCoach();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!sharedPreferences.getBoolean(Constants.SHARED_PREFS_DATABASE_EXISTS, false)) {
            DatabaseInitializer initializer = new DatabaseInitializer(this);
            initializer.initializeDatabase();
        }
    }

    private void loadImage(String src) {
        Picasso.with(this)
                .load(src)
                .into(imageView);
    }

    @Override
    public void databaseInitialized(Boolean result) {
        if (result) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            sharedPreferences.edit().putBoolean(Constants.SHARED_PREFS_DATABASE_EXISTS, true).apply();
            onClickGetCoach();
        } else {
            Toast.makeText(this, R.string.database_init_error, Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.button_add_pokemon)
    public void onClickAddPokemon() {
        Pokemon pokemon = new Pokemon();
        pokemon.setName("Pika");
        pokemon.setType(Pokemon.Type.ELECTRIC);
        pokemon.setLevel(1);
        mPokemon = pokemon;
        new PokemonTask(this, Constants.SAVE_POKEMON_TASK).execute(pokemon);
    }

    @OnClick(R.id.button_update_pokemon)
    public void onClickUpdatePokemon() {
        mPokemon.setLevel(999);
        new PokemonTask(this, Constants.SAVE_POKEMON_TASK).execute(mPokemon);
    }

    @OnClick(R.id.button_delete_pokemon)
    public void onClickDeletePokemon() {
        new PokemonTask(this, Constants.DELETE_POKEMON_TASK).execute(mPokemon);
    }

    @OnClick(R.id.button_get_pokemon)
    public void onClickGetPokemon() {
        new PokemonsTask(this).execute();
    }

    @OnClick(R.id.button_add_coach)
    public void onClickAddCoach() {
        Coach coach = new Coach();
        coach.setName("Coach");
        mCoach = coach;
        new CoachTask(this, Constants.SAVE_COACH_TASK).execute(coach);
    }

    @OnClick(R.id.button_update_coach)
    public void onClickUpdateCoach() {
        mCoach.setName("CoachCoach");
        new CoachTask(this, Constants.SAVE_COACH_TASK).execute(mCoach);
    }

    @OnClick(R.id.button_delete_coach)
    public void onClickDeleteCoach() {
        new CoachTask(this, Constants.DELETE_COACH_TASK).execute(mCoach);
    }

    @OnClick(R.id.button_get_coach)
    public void onClickGetCoach() {
        new CoachesTask(this).execute();
    }

    @OnClick(R.id.button_add_pc)
    public void onClickAddPokemonCoach() {
        PokemonCoach pokemonCoach = new PokemonCoach();
        pokemonCoach.setPokemon(mPokemon);
        pokemonCoach.setCoach(mCoach);
        mPokemonCoach = pokemonCoach;
        new PokemonCoachTask(this, Constants.SAVE_POKEMON_COACH_TASK).execute(pokemonCoach);
    }

    @OnClick(R.id.button_delete_pc)
    public void onClickDeletePokemonCoach() {
        new PokemonCoachTask(this, Constants.DELETE_POKEMON_COACH_TASK).execute(mPokemonCoach);
    }

    @Override
    public void onPokemonSaved(Boolean result) {
        onClickGetPokemon();
    }

    @Override
    public void onPokemonDeleted() {
        onClickGetPokemon();
    }

    @Override
    public void onCoachSaved(Boolean result) {
        onClickGetCoach();
    }

    @Override
    public void onCoachDeleted() {
        onClickGetCoach();
    }

    @Override
    public void onPokemonCoachSaved(Boolean result) {
        onClickGetCoach();
    }

    @Override
    public void onPokemonCoachDeleted() {
        onClickGetCoach();
    }

    @Override
    public void onPokemonsSelected(List<Pokemon> pokemons) {
        pokemonTextView.setText("Pokemons:\n");
        for (Pokemon pokemon : pokemons) {
            pokemonTextView.append(pokemon.getName() + ", " +
                    pokemon.getLevel() + ", " +
                    pokemon.getImageSrc() + ", " +
                    pokemon.getType() + "\n\n\n");
        }
        loadImage(pokemons.get(0).getImageSrc());
    }

    @Override
    public void onCoachesSelected(List<Coach> coaches) {
        pokemonTextView.setText("Database:\n");
        if (!coaches.isEmpty()) {
            for (Coach coach : coaches) {
                pokemonTextView.append(coach.getName() + ":\n");
                if (!coach.getPokemons().isEmpty()) {
                    for (Pokemon pokemon : coach.getPokemons()) {
                        pokemonTextView.append(pokemon.getName() + ", " +
                                pokemon.getLevel() + ", " +
                                pokemon.getImageSrc() + ", " +
                                pokemon.getType() + "\n\n\n");
                    }
                }
            }
        } else {
            pokemonTextView.setText("Is empty? LOL");
        }
    }
}
